const express = require('express');
const mongoose = require('mongoose');
const path = require('path');
const cors = require('cors');

const app = express();
const server = require('http').Server(app);
const io = require('socket.io')(server); // faz com que o server aceita conexões http e websocket
// websocket = permite comunicação em tempo real

mongoose.connect('mongodb+srv://semana:omnistack@cluster0-nekqc.mongodb.net/test?retryWrites=true&w=majority',{
  useNewUrlParser: true
});

// middleware personalizado para incluir o IO em todas as requisições
app.use((req, res, next) => {
  req.io = io; // enviar io para todas as rotas
  next();
});

app.use(cors()); // permite que todos os endereços possam acessar o backend
app.use('/files', express.static(path.resolve(__dirname, '..', 'uploads', 'resized'))); // acessar arquivos estáticos
app.use(require('./routes')); // rotas

server.listen(3333, () => {
  console.log('Exeutando API...');
});