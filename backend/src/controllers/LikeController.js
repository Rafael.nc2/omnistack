const Post = require('../models/Post');

module.exports = {
  async store(req, res) {
    const post = await Post.findById(req.params.id);

    // aumenta a quantidade de likes do post
    post.likes += 1;

    await post.save();

    // envia o post inserido através do socket para todos os usuários conectados na aplicação
    req.io.emit('like', post);

    return res.json(post);
  }
};