const Post = require('../models/Post');
const sharp = require('sharp');
const path = require('path');
const fs = require('fs');

module.exports = {
  async index(req, res) {
    const posts = await Post.find().sort('-createdAt'); //order desc

    return res.json(posts);
  },

  async store(req, res) {
    const { author, place, description, hashtags } = req.body;
    const { filename: image } = req.file;

    const [name] = image.split('.');
    const fileName = `${name}.jpg`; // renomeia o arquivo para .jpg

    // redimensiona a imagem original
    await sharp(req.file.path)
      .resize(500) // px em largura ou altura
      .jpeg({ quality: 70 })
      .toFile(
        path.resolve(req.file.destination, 'resized', fileName)
      );

      fs.unlinkSync(req.file.path); // apaga a imagem original

    const post = await Post.create({
      author,
      place,
      description,
      hashtags,
      image: fileName
    });

    // envia o post inserido através do socket para todos os usuários conectados na aplicação
    req.io.emit('post', post);

    return res.json(post);
  }
};