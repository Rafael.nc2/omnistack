const express = require('express');
const multer = require('multer'); // permite o express entender multpart-formdata (upload)
const uploadConfg = require('./config/upload');

const PostController = require('./controllers/PostControler');
const LikeController = require('./controllers/LikeController');

const routes = new express.Router();
const upload = multer(uploadConfg);

routes.get('/posts', PostController.index);
routes.post('/posts', upload.single('image'), PostController.store);

routes.post('/posts/:id/like', LikeController.store); // realiza likes no post

module.exports = routes;